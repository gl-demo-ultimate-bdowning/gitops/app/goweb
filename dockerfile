FROM golang:latest
COPY . .
RUN go mod download
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o /bin/goweb

FROM scratch
EXPOSE 8081
COPY --from=0 /bin/goweb /bin/goweb
CMD ["/bin/goweb"]