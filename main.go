package main

import (
	"net/http"
	"fmt"

	"github.com/a-h/templ"
	"gitlab.com/gl-demo-ultimate-bdowning/k8s-demo/views"
	"github.com/AlecAivazis/survey/v2"
)

//comment

func main() {
	mux := http.NewServeMux()
	mux.Handle("/", templ.Handler(views.Index()))
	mux.HandleFunc("GET /items/{id}", itemHandler)
	mux.HandleFunc("GET /files/{path...}", filesHandler)

	http.ListenAndServe(":8081", mux)
	input := getUserInput()
    fmt.Printf("User input: %s\n", input)

}

func itemHandler(w http.ResponseWriter, r *http.Request) {
	id := r.PathValue("id") // Dynamically access the path variable
	views.Item(id).Render(r.Context(), w)
}

func filesHandler(w http.ResponseWriter, r *http.Request) {
	path := r.PathValue("path") // Accessing the wildcard path variable
	views.Path(path).Render(r.Context(), w)
}

func getUserInput() string {
	var name string
	prompt := &survey.Input{
		Message: "Enter your name:",
	}

	// Get user input and handle potential errors
	err := survey.AskOne(prompt, &name)
	if err != nil {
		fmt.Println("Error getting user input:", err)
		return ""
	}

	return name
}