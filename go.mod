module gitlab.com/gl-demo-ultimate-bdowning/k8s-demo

go 1.23

toolchain go1.23.5

require github.com/a-h/templ v0.3.819

require (
	github.com/AlecAivazis/survey/v2 v2.3.7
	github.com/manifoldco/promptui v0.9.0
)

require (
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e // indirect
	github.com/kballard/go-shellquote v0.0.0-20180428030007-95032a82bc51 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/mgutz/ansi v0.0.0-20170206155736-9520e82c474b // indirect
	golang.org/x/sys v0.28.0 // indirect
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211 // indirect
	golang.org/x/text v0.4.0 // indirect
)
